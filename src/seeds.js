import mongoose, { Mongoose } from 'mongoose';
import BackgammonBoard from './models/backgammonBoard';
import Comment from './models/comment';

var data = [
  {
    name: "Wooden",
    image: "https://tinyurl.com/y4nxyfzs",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
  },
  {
    name: "P40",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwMWDoBfm9zyFDfmmul57mAJ9AOnuqHGhN7dO7w8jxCRPL1-tSlQ",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
  },
  {
    name: "Color",
    image: "https://tinyurl.com/yyn7unw5",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
  }
]

function seedDB() {
  //Remove all boards
  BackgammonBoard.remove({}, (err) => {
    if (err) {
      console.log(err);
    }
    console.log("removed all boards!");
    Comment.remove({}, (err) => {
      if (err) {
        console.log(err);
      }
      console.log("removed comments!");
      //add a few boards
      data.forEach(function (seed) {
        BackgammonBoard.create(seed, (err, board) => {
          if (err) {
            console.log(err)
          } else {
            console.log("added a board");
            //create a comment
            Comment.create(
              {
                text: "This board is great",
                author: "Magriel"
              }, (err, comment) => {
                if (err) {
                  console.log(err);
                } else {
                  board.comments.push(comment);
                  board.save();
                  console.log("Created new comment");
                }
              });
          }
        });
      });
    });
  });
  //add a few comments
}

module.exports = seedDB;