import express from 'express';
import passport from 'passport';
import User from '../models/user';
let router = express.Router();


//root route
router.get('/', (req, res) => {
  res.render('landing');
});

// AUTHENTICATION 
//show registration form
router.get('/register', (req, res) => {
  res.render('register');
});

// sign up logic
router.post('/register', (req, res) => {
  User.register(new User({ username : req.body.username}), req.body.password, (err, user) => {
      if(err) {
          console.log(err);
          return res.redirect('/register');
      }
      passport.authenticate('local')(req, res, () => {
          res.redirect('/backgammonBoards');
      });
  });
});

//show login form
router.get('/login', (req, res) => {
  res.render('login');
});

// handling login 
router.post('/login', passport.authenticate('local', {
  successRedirect: '/backgammonBoards',
  failureRedirect: '/login'
}), (req, res) => {

});

//logout route
router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/backgammonBoards');
});

// middleware
function isLoggedIn(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

module.exports = router;