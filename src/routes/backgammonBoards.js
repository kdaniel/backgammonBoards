import express from 'express';
import BackgammonBoard from '../models/backgammonBoard';

let router = express.Router();

// index page show all boards
router.get('/', (req, res) => {
  BackgammonBoard.find({}, (err, allBoards) => {
    if (err) {
      console.log(err);
    } else {
      res.render('backgammonBoard/index', { backgammonBoards: allBoards, currentUser: req.user });
    }
  })
});

router.post('/', isLoggedIn, (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let description = req.body.description;
  let author = {
    id: req.user._id,
    username: req.user.username
  };
  let board = { name, image, description, author };
  BackgammonBoard.create(board, (err, newBoard) => {
    if (err) {
      console.log(err);
    } else {
      //default redirect is the get if more is defined
      res.redirect("/backgammonBoards");
    }
  })
});

router.get('/new', isLoggedIn, (req, res) => {
  res.render('backgammonBoard/new');
});

router.get('/:id', (req, res) => {
  BackgammonBoard.findById(req.params.id).populate('comments').exec((err, foundBoard) => {
    if (err) {
      console.log(err);
    } else {
      res.render('backgammonBoard/show', { backgammonBoard: foundBoard });
    }
  })
});

// EDIT
router.get('/:id/edit', checkBackgammonBoardsOwnership, (req, res) => {
  BackgammonBoard.findById(req.params.id, (err, foundBackgammonBoard) => {
    res.render('backgammonBoard/edit', { backgammonBoard: foundBackgammonBoard });
  });
});
// UPDATE
router.put('/:id', checkBackgammonBoardsOwnership, (req, res) => {
  BackgammonBoard.findByIdAndUpdate(req.params.id, req.body.backgammonBoard, (err, updatedBackgammonBoard) => {
    if (err) {
      res.redirect('/backgammonBoards');
    } else {
      res.redirect(`/backgammonBoards/${req.params.id}`);
    }
  });
});

//DELETE
router.delete('/:id', checkBackgammonBoardsOwnership, (req, res) => {
  BackgammonBoard.findByIdAndRemove(req.params.id, err => {
    if (err) {
      res.redirect('/backgammonBoards');
    } else {
      res.redirect('/backgammonBoards');
    }
  });
});


function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

function checkBackgammonBoardsOwnership(req, res, next) {
  if (req.isAuthenticated()) {
    BackgammonBoard.findById(req.params.id, (err, foundBackgammonBoard) => {
      if (err) {
        res.redirect('back');
      } else {
        // does user own the board?
        if (foundBackgammonBoard.author.id.equals(req.user._id)) {
          next();
        } else {
          res.redirect('back');
        }
      }
    });
  } else {
    res.redirect('back');
  }
}

module.exports = router;