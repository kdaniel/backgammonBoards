import express from 'express';
import BackgammonBoard from '../models/backgammonBoard'
import Comment from '../models/comment'

let router = express.Router({ mergeParams: true });

//comments new
router.get('/new', isLoggedIn, (req, res) => {
  BackgammonBoard.findById(req.params.id, (err, foundBoard) => {
    if (err) {
      console.log(err);
    } else {
      res.render('comments/new', { backgammonBoard: foundBoard });
    }
  });
});
// page is hidden for not logged in users, but without isloggedin post still could be valid
// comments create
router.post('/', isLoggedIn, (req, res) => {
  BackgammonBoard.findById(req.params.id, (err, foundBoard) => {
    if (err) {
      console.log(err);
    } else {
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          console.log(err);
        } else {
          //add username and id
          comment.author.id = req.user._id;
          comment.author.username = req.user.username;
          comment.save();
          //save comment
          foundBoard.comments.push(comment);
          foundBoard.save();
          res.redirect(`/backgammonBoards/${foundBoard._id}`);
        }
      });
    }
  });
});
// comments edit route
router.get('/:comment_id/edit', checkCommentOwnership, (req, res) => {
  Comment.findById(req.params.comment_id, (err, foundComment) => {
    if (err) {
      res.redirect('back');
    } else {
      res.render('comments/edit', { backgammonBoard_id: req.params.id, comment: foundComment });
    }
  });
});

// comments update route
router.put('/:comment_id', checkCommentOwnership, (req, res) => {
  Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, foundComment) => {
    if(err) {
      res.redirect('back');
    } else {
        res.redirect(`/backgammonBoards/${req.params.id}`);
    }
  });
});
// delete comment
router.delete('/:comment_id', checkCommentOwnership, (req, res) => {
  Comment.findByIdAndRemove(req.params.comment_id, (err) => {
    if(err) {
      res.redirect('back');
    } else {
      res.redirect(`/backgammonBoards/${req.params.id}`);
    }
  });
});

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

function checkCommentOwnership(req, res, next) {
  if (req.isAuthenticated()) {
    Comment.findById(req.params.comment_id, (err, foundComment) => {
      if (err) {
        res.redirect('back');
      } else {
        // does user own the board?
        if (foundComment.author.id.equals(req.user._id)) {
          next();
        } else {
          res.redirect('back');
        }
      }
    });
  } else {
    res.redirect('back');
  }
}

module.exports = router;