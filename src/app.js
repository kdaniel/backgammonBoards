import express from 'express';
import bodyParser from 'body-parser';
import mongoose, { Mongoose } from 'mongoose';
import LocalStrategy from 'passport-local';
import passport from 'passport';
import methodOverride from 'method-override';
import BackgammonBoard from './models/backgammonBoard';
import Comment from './models/comment'
import seedDB from './seeds';
import User from './models/user';
import commentRoutes from './routes/comments'
import backgammonBoardRoutes from './routes/backgammonBoards'
import indexRoutes from './routes/index'

// seedDB();
const app = express();

mongoose.connect("mongodb://localhost:27017/backgammon", { useNewUrlParser: true });

app.use(bodyParser.urlencoded({ extended: true }))
// emiatt nem kell az ejs kiterjesztes a res.renderbe
app.set('view engine', 'ejs');

app.use(express.static('src/public'));
app.use(methodOverride('_method'));


//Config Passport
app.use(require("express-session")({
  secret: "lolka",
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// middleware function to pass user to all pages
app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  next();
});

app.use(indexRoutes);
app.use('/backgammonBoards/:id/comments', commentRoutes);
app.use('/backgammonBoards', backgammonBoardRoutes);


app.listen(3000, (req, res) => {
  console.log('App is running...');
});