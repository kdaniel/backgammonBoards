import mongoose from 'mongoose';
// import { type } from 'os';

//SCHEMA
let backgammonBoardSchema = mongoose.Schema({
  name: String,
  image: String,
  description: String, 
  author: {
    id: {
      type : mongoose.Schema.Types.ObjectId,
      ref : 'User'
    }, 
    username: String
  },
  comments: [
    {
      type : mongoose.Schema.Types.ObjectId,
      ref : "Comment"
    }
    
  ]
});

var BackgammonBoard = mongoose.model('BackgammonBoards', backgammonBoardSchema);

module.exports = mongoose.model('BackgammonBoard', backgammonBoardSchema);
